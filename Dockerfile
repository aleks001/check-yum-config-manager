FROM registry.access.redhat.com/rhel7:latest


RUN set -x && \
    yum clean all && \
    yum-config-manager --enable rhel-7-server-ose-3.2-rpms && \ 
    yum-config-manager --enable rhel-7-server-rpms && \
    yum repolist && \
    INSTALL_PKGS=socat && \
    yum search fio && \
    yum install -y $INSTALL_PKGS && \
    rpm -V $INSTALL_PKGS && \
    yum clean all  && \
    localedef -f UTF-8 -i en_US en_US.UTF-8

# ENTRYPOINT ["/bin/sh]
CMD while  true; do echo hello world; sleep 30; done